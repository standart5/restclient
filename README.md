
https://standart5@bitbucket.org/standart5/restclient.git

RestSonda - projekt wykonany w technologi Xamarin.Forms

Działanie aplikacji polega na, odpytaniu, na żadanie użytkownika, restowego API, do którego to, użytkownik podał łącze, w procesie tworzenia Objektu zwanego RestApi.
Objekt RestApi, przetrzymuje wszystkie informacje potrzebne do połączenia z API. 
Na podstawie uzyskanych rezultatów, generowane są grafy prezentujące relacje między poszczególnymi wywołaniami. 

Repozytorium zawiera suche rozwiązanie danego zagadnienia, to jest kod. 
Aby uruchomić aplikację,  należy użyć do tego narzędzia wspierającego technologię Xamarin.Forms. 

Ponad to, aplikacja była testowana tylko na urządzeniu – Xiaomi Readmi Note 4  - android 7

Cały projekt znajduje się na branch'u Test

W wiki, zostały zaprezentowane poszczególne etapy działania aplikacji.