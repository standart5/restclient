﻿using RestSonda.persistence;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

using Xamarin.Forms;

namespace RestSonda
{
	public partial class App : Application
	{
        RestApiList restApiList;
        public App ()
		{
			InitializeComponent();
            DBService.connectToDB();
            restApiList = new RestApiList();
        }
        public Page GetMainPage()
        {
            return new NavigationPage(restApiList);
        }
        protected override void OnStart ()
		{
            MainPage = GetMainPage();
            // Handle when your app starts
        }

        protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
