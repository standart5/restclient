﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using RestSonda.persistence.entities;
using RestSonda.persistence.repository;
using RestSonda.src;

namespace RestSonda
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateRestApi : ContentPage
    {
        RestApi restApi;

        public CreateRestApi()
        {
            InitializeComponent();
            restApi = new RestApi();
            BindingContext = restApi;
            saveMenuItem.Clicked += OnSaveClicked;
        }

        public CreateRestApi(int apiId)
        {
            InitializeComponent();
            this.restApi = RestApiDAO.getById(apiId);
            BindingContext = restApi;
            Switch isPost = this.FindByName<Switch>("isPost");
            isPost.IsToggled = restApi.METHODE == "POST";
            saveMenuItem.Clicked += OnUpdateClicked;
        }

        void OnSaveClicked(object sender, EventArgs e)
        {
            changeVisibleOfErroLabels();
            if (valid())
                return;

            Switch isPost = this.FindByName<Switch>("isPost");
            restApi.METHODE = (isPost.IsToggled) ? "POST" : "GET";
            restApi.METHODE_IMAGE = restApi.METHODE + ".png";
            restApi.CREATIONDATE = DateTime.Now;
            restApi.MODIFYDATE = DateTime.Now;
            RestApiDAO.create(restApi);
            Navigation.PopAsync();
        }

        void OnUpdateClicked(object sender, EventArgs e)
        {
            changeVisibleOfErroLabels();
            if (valid())
                return;

            Switch isPost = this.FindByName<Switch>("isPost");
            restApi.METHODE = (isPost.IsToggled) ? "POST" : "GET";
            restApi.METHODE_IMAGE = restApi.METHODE + ".png";
            restApi.MODIFYDATE = DateTime.Now;
            RestApiDAO.update(restApi);
            Navigation.PopAsync();
        }

        public void changeVisibleOfErroLabels()
        {
            emailErrorLabel.IsVisible = false;
            nameErrorLabel.IsVisible = false;
            urlErrorLabel.IsVisible = false;
        }

        public bool valid()
        {
            bool isInCorrectEmail = !validEmail();
            bool isInCorrectName = !validName();
            bool isInCorrectEURL = !validURL();
            return isInCorrectEmail || isInCorrectEURL || isInCorrectName;  
        }

        public bool validEmail() {
            String filed = "Email";

            if (ValidatorUtil.isEmpty(restApi.EMAIL)) {
                emailErrorLabel.Text = string.Format(ValidatorUtil.EmptyMessage, filed);
                emailErrorLabel.IsVisible = true;
                return false;
            }
            if (!ValidatorUtil.isCorrectEmail(restApi.EMAIL))
            {
                emailErrorLabel.Text = string.Format(ValidatorUtil.ErrorMessage, filed);
                emailErrorLabel.IsVisible = true;
                return false;
            }
            return true;
        }
        
        public bool validName()
        {
            String filed = "Name";
            if (ValidatorUtil.isEmpty(restApi.NAME))
            {
                nameErrorLabel.Text = string.Format(ValidatorUtil.EmptyMessage, filed);
                nameErrorLabel.IsVisible = true;
                return false;
            }
            return true;
        }

        public bool validURL()
        {
            String filed = "URL";
            if (ValidatorUtil.isEmpty(restApi.URL))
            {
                urlErrorLabel.Text = string.Format(ValidatorUtil.EmptyMessage, filed);
                urlErrorLabel.IsVisible = true;
                return false;
            }
            if (!ValidatorUtil.isCorrectURL(restApi.URL))
            {
                urlErrorLabel.Text = string.Format(ValidatorUtil.ErrorMessage, filed);
                urlErrorLabel.IsVisible = true;
                return false;
            }
            return true;
        }
     
        

    }
}