﻿using RestSharp;
using RestSonda.persistence.entities;
using RestSonda.persistence.repository;
using RestSonda.src;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RestSonda
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CreateRestApiParam : ContentPage
	{
        RestApi restApi;
        Pair pair { get; set; }
        List<String> parametersType;
        public CreateRestApiParam()
		{
            InitializeComponent();
            layoutParamType.IsVisible = false;
        }
        public CreateRestApiParam(RestApi restApi, Boolean isHeader)
        {
            init(restApi);
            
            if (isHeader)
            {
                this.pair = new Headers();
                saveMenuItem.Clicked += HeaderOnSaveClicked;
            }
            else
            {
                this.pair = new Parameters();
                loadPrametersTypePicker();
                saveMenuItem.Clicked += ParameterOnSaveClicked;
            }
            BindingContext = pair;
        }
        public CreateRestApiParam(RestApi restApi, Headers header)
        {
            init(restApi);
            this.pair = header;
            BindingContext = pair;
            saveMenuItem.Clicked += HeaderOnUpdateClicked;
        }
        public CreateRestApiParam(RestApi restApi, Parameters parameter)
        {
            init(restApi);
            this.pair = parameter;
            BindingContext = pair;
            layoutParamType.IsVisible = true;
            loadPrametersTypePicker();
            if (pair.TYPE != null)
                  pickerParamType.SelectedIndex = parametersType.IndexOf(pair.TYPE);
            saveMenuItem.Clicked += ParameterOnUpdateClicked;
        }

        void init(RestApi restApi) { 
            InitializeComponent();
            this.restApi = restApi;
            layoutParamType.IsVisible = false;
        }
        void loadPrametersTypePicker() {
            parametersType = new List<String>();
            parametersType.Add("Cookie");
            parametersType.Add("GetOrPost");
            parametersType.Add("HttpHeader");
            parametersType.Add("QueryString");
            parametersType.Add("RequestBody");
            parametersType.Add("UrlSegment");
            pickerParamType.ItemsSource = parametersType;
            layoutParamType.IsVisible = true;
        }

        void HeaderOnUpdateClicked(object sender, EventArgs e)
        {
            changeVisibleOfErroLabels();
            if (!validKey())
                return;

            if (this.restApi != null && pair.KEY != null && !pair.KEY.Trim().Equals(""))
            {
                HeadersDAO.update((Headers)pair);
                Navigation.PopAsync();
            }
        }

        void HeaderOnSaveClicked(object sender, EventArgs e)
        {
            changeVisibleOfErroLabels();
            if (!validKey())
                return;

            if (this.restApi != null && pair.KEY != null && !pair.KEY.Trim().Equals(""))
            {
                HeadersDAO.create(this.restApi.Id, pair.KEY, pair.VALUE);
                Navigation.PopAsync();
            }
        }

        void ParameterOnUpdateClicked(object sender, EventArgs e)
        {
            changeVisibleOfErroLabels();
            if (!validKey())
                return;

            if (this.restApi != null && pair.KEY != null && !pair.KEY.Trim().Equals(""))
            {
                pair.TYPE = parametersType[pickerParamType.SelectedIndex];
                ParametersDAO.update((Parameters)pair);
                Navigation.PopAsync();
            }
        }

        void ParameterOnSaveClicked(object sender, EventArgs e)
        {
            changeVisibleOfErroLabels();
            if (!validKey())
                return;

            if (this.restApi != null && pair.KEY != null && !pair.KEY.Trim().Equals(""))
            {
                pair.TYPE = parametersType[pickerParamType.SelectedIndex];
                ParametersDAO.create(this.restApi.Id, pair.KEY, pair.VALUE, pair.TYPE);
                Navigation.PopAsync();
            }
        }

        public void changeVisibleOfErroLabels()
        {
            keyErrorLabel.IsVisible = false;
        }

        public bool validKey()
        {
            String field = "Key";
            if (ValidatorUtil.isEmpty(pair.KEY))
            {
                keyErrorLabel.Text = string.Format(ValidatorUtil.EmptyMessage, "field");
                keyErrorLabel.IsVisible = true;
                return false;
            }
            return true;
        }
        
    }
}