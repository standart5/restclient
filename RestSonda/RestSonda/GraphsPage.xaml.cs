﻿using RestSonda.persistence.entities;
using RestSonda.persistence.repository;
using RestSonda.src;
using Microcharts;
using SkiaSharp;
using Microcharts.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using Xamarin.Forms.Xaml;
using Entry = Microcharts.Entry;
namespace RestSonda
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GraphsPage : Xamarin.Forms.ContentPage
    {
        
        RestApi restApi;
        ObservableCollection<Results> results = new ObservableCollection<Results>();
        Dictionary<String,SKColor> colorsPerStatus;
        Dictionary<String, SKColor> colorsPerDay;

        public ResultStats resultStats { get; set; }

        public GraphsPage()
        {
            InitializeComponent();
        }

        public GraphsPage(RestApi restApi)
        {
            InitializeComponent();
            this.restApi = restApi;
        }

        public async void init(ObservableCollection<Results> list)
        {
            results = list;
            messageLabel.Text = string.Format("Amount of all Queries - {0}", results.Count);
            colorsPerStatus = new Dictionary<String, SKColor>();
            colorsPerDay = new Dictionary<String, SKColor>();
            resultStats = new ResultStats(results);
            await generateColorList();
            await generatePlots();
        }

        async Task generatePlots()
        {
            PlotPerStatusCode.Chart = await CreatePlotPerStatusCode();
            DailyPlotPerStatusCode.Chart = await CreateDailyPlotPerStatusCode();
            PlotStausCodePerHour.Chart = await CreatePlotStausCodePerHour();
            WeekConnectionPlot.Chart = await CreateWeekConnectionPlot();
        }

        async Task<Chart> CreatePlotPerStatusCode()
        {
            if (resultStats.resultsPerStatusCode.Count > 0)
                LayotPlotPerStatusCode.IsVisible = true;
            else
                LayotPlotPerStatusCode.IsVisible = false;

            return new RadialGaugeChart()
            {
                Entries = await createEntryList(resultStats.resultsPerStatusCode, colorsPerStatus),
                LabelTextSize = 40,
            };
        }

        async Task<Chart> CreateDailyPlotPerStatusCode()
        {
            if (resultStats.dailyResultsPerStatusCode.Count > 0)
                LayotDailyPlotPerStatusCode.IsVisible = true;
            else
                LayotDailyPlotPerStatusCode.IsVisible = false;

            return new DonutChart()
            {
                Entries = await createEntryList(resultStats.dailyResultsPerStatusCode, colorsPerStatus),
                LabelTextSize = 40,
                HoleRadius = 0.5F,
                Margin = 100
            };
        }

        async Task<Chart> CreateWeekConnectionPlot()
        {
            if (resultStats.resultsAmountPerWeek.Count > 0)
                LayotWeekConnectionPlot.IsVisible = true;
            else
                LayotWeekConnectionPlot.IsVisible = false;

            return new LineChart()
            {
                Entries = await createEntryList(resultStats.resultsAmountPerWeek, colorsPerDay),
                LineMode = LineMode.Straight,
                LineSize = 8,    
                LabelTextSize = 40,
                Margin = 30,
                PointMode = PointMode.Square,
                PointSize = 18
                
            };
        }
        async Task<Chart> CreatePlotStausCodePerHour()
        {
            if (resultStats.resultsFromLastTenHours.Count > 0)
                LayotPlotStausCodePerHour.IsVisible = true;
            else
                LayotPlotStausCodePerHour.IsVisible = false;

            List<Entry> entries = new List<Entry>();
            List<int> usedHour = new List<int>();
            foreach (var stat in resultStats.resultsFromLastTenHours)
            {
                try
                {
                    String label = "";
                    if (stat.EXECUTEDATE != null && !usedHour.Contains(stat.EXECUTEDATE.Hour))
                    {
                        label = stat.EXECUTEDATE.Hour.ToString();
                        usedHour.Add(stat.EXECUTEDATE.Hour);
                    }
                    String colorLabel = (stat.STATUSCODEDESCRIPTION == null) ? "" : stat.STATUSCODEDESCRIPTION;
                    entries.Add(createEntry(label, stat.STATUSCODE, stat.STATUSCODE.ToString(), colorsPerStatus[colorLabel]));
                }
                catch (Exception ex) { Console.WriteLine(ex); }
            }
            return new LineChart()
            {
                Entries = entries, 
                LabelTextSize = 40,
                Margin = 30,
                PointSize = 18
            };
        }

        async Task<List<Entry>> createEntryList(Dictionary<String, int> resultsStatMap, Dictionary<String, SKColor> colors)
        {
            List<Entry> entries = new List<Entry>();
            foreach (var stat in resultsStatMap)
            {
                String colorLabel = (stat.Key == null) ? "" : stat.Key;
                entries.Add(createEntry(stat.Key, stat.Value, stat.Value.ToString(), colors[colorLabel]));
            }
            return entries;
        }
               
        Entry createEntry(String label, int value, String valueLabel, SKColor color)
        {
            Entry entry = new Entry(value)
            {
                Color = color,
                Label = (label==null)?"":label,
                ValueLabel = (valueLabel == null) ? "" : valueLabel
            };
            return entry;
        }
        async Task generateColorList()
        {
            foreach (var stat in resultStats.resultsPerStatusCode)
            {
                String label = (stat.Key == null) ? "" : stat.Key;
                addColorToList(stat.Key, colorsPerStatus);
            }
            foreach (var stat in resultStats.resultsAmountPerWeek)
            {
                addColorToList(stat.Key, colorsPerDay);
            }
        }

        void addColorToList(String key,Dictionary<String,SKColor> colors)
        {
            SKColor color = getColor();
            if (!colors.ContainsValue(color))
            {
                colors.Add(key,color);
            }
            else
            {
                addColorToList(key, colors);
            }
        }


        SKColor getColor()
        {
            var random = new Random();
            var color = String.Format("#{0:X6}", random.Next(0x1000000));
            return SKColor.Parse(color);
        }
     }

}