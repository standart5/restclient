﻿using RestSonda.persistence;
using RestSonda.persistence.entities;
using RestSonda.persistence.repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using SQLite;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RestSonda
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RestApiList : ContentPage
    {
        ObservableCollection<RestApi> resApiListItems = new ObservableCollection<RestApi>();
        public RestApiList()
        {
            InitializeComponent();
            this.Appearing += OnAppearing;
        }

        async void OnAppearing(object sender, EventArgs args)
        {
            loadApiList();
        }

        public void loadApiList()
        {
            resApiListItems = new ObservableCollection<RestApi>(RestApiDAO.getAll());
            RestListView.ItemsSource = resApiListItems;
        }

        async void NavigateToRestApiPage(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;

            RestApi api =(RestApi)((ListView)sender).SelectedItem;

            ((ListView)sender).SelectedItem=null;
            await Navigation.PushAsync(new RestApiTabPage(api.Id));

            //await Navigation.PushModalAsync(new RestApiPage(api.Id));
        }
      
        async void NavigeateToCreatePage(object sender, ItemTappedEventArgs e)
        {
            await Navigation.PushAsync(new CreateRestApi());
        }

        public void OnDelete(object sender, EventArgs e)
        {
            RestApi restApi = ((RestApi)((MenuItem)sender).CommandParameter);
            RestApiDAO.delete(restApi);
            loadApiList();
        }
        
    }
}
