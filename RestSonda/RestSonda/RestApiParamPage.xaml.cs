﻿using RestSonda.persistence.entities;
using RestSonda.persistence.repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RestSonda
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RestApiParamPage : ContentPage
    {
        RestApi restApi;
        ObservableCollection<Pair> param = new ObservableCollection<Pair>();
        bool isHeaders =false;
        public RestApiParamPage()
        {
            InitializeComponent();
        }
        public RestApiParamPage(bool isHeaders,RestApi restApi)
        {
            InitializeComponent();
            this.restApi = restApi;
            this.isHeaders = isHeaders;
            this.Appearing += OnAppearing;

        }
        async void OnAppearing(object sender, EventArgs args)
        {
            loadParamList();
        }

        public void loadParamList() {
            if (restApi == null)
                return;

            if (isHeaders)
            {
                loadParamListAsHeaders();
            }
            else
            {
                loadParamListAsParameters();
            }
        }
      

        async void NavigateToUpdate(object sender, EventArgs e)
        {
            if (isHeaders)
            {
               await Navigation.PushAsync(new CreateRestApiParam(restApi, (Headers)((MenuItem)sender).CommandParameter));
            }
            else
            {
               await Navigation.PushAsync(new CreateRestApiParam(restApi, (Parameters)((MenuItem)sender).CommandParameter));
            }

        }

        async void NavigateToCreate(object sender, EventArgs e)
        {
            try
            {
               await Navigation.PushAsync(new CreateRestApiParam(restApi, isHeaders));
            }
            catch (Exception se) {
                Console.WriteLine(se);
            }
        }

        public void OnDelete(object sender, EventArgs e)
        {
            if (this.isHeaders)
            {
                deleteHeader(((Headers)((MenuItem)sender).CommandParameter));
            }
            else
            {
                deleteParameter(((Parameters)((MenuItem)sender).CommandParameter));
            }
        }

        //Functions of headers
        public void loadParamListAsHeaders()
        {
            param = new ObservableCollection<Pair>(HeadersDAO.getByRestApiId(restApi.Id));
            ParamList.ItemsSource = param;
        }
        void deleteHeader(Headers header)
        {
            HeadersDAO.delete(header);
            loadParamListAsHeaders();
        }
        
        //Functions of parameters
        public void loadParamListAsParameters()
        {
            param = new ObservableCollection<Pair>(ParametersDAO.getByRestApiId(restApi.Id));
            ParamList.ItemsSource = param;
        }
        void deleteParameter(Parameters parameters)
        {
            ParametersDAO.delete(parameters);
            loadParamListAsParameters();
        }
    }
}