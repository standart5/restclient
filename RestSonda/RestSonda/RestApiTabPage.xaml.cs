﻿using RestSonda.persistence.entities;
using RestSonda.persistence.repository;
using RestSonda.src;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RestSonda
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RestApiTabPage : TabbedPage
    {
        static RestApi restApi;
        ResultList resultList;
        GraphsPage graphsPage;
        ObservableCollection<Results> results = new ObservableCollection<Results>();

        public RestApiTabPage ()
        {
            InitializeComponent();
        }

        public RestApiTabPage(int restApiId)
        {
            InitializeComponent();
            try
            {
                init(restApiId);
                this.Appearing += OnAppearing;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        async void init(int restApiId)
        {
            restApi = RestApiDAO.getById(restApiId);

            this.resultList = new ResultList(restApi) { Title = "List" };
            this.Children.Add(resultList);
                
            this.graphsPage = new GraphsPage(restApi) { Title = "Stats" };
            this.Children.Add(graphsPage);

            this.Children.Add(new RestApiParamPage(true, restApi) { Title = "Headers" });

            this.Children.Add(new RestApiParamPage(false, restApi) { Title = "Params" });
        }

        async void OnAppearing(object sender, EventArgs args)
        {
            restApi = RestApiDAO.getById(restApi.Id);
            await loadResults();

            if (resultList == null || graphsPage == null)
                return;

            this.resultList.init(results);
            this.graphsPage.init(results);
        }

        async void Connect(object sender, ItemTappedEventArgs e)
        {
            if (restApi == null)
                return;

            try
            { 
                RestClientConnector restClientConnector = new RestClientConnector(restApi);
                await restClientConnector.runConnection();
            }
            catch (Exception ex)
            {
                Console.WriteLine("RestClientConnector Error");
            }

            await loadResults();

            if (this.resultList != null)
                this.resultList.init(results);
            if (this.graphsPage != null)
                this.graphsPage.init(results);
        }

        async void Edit(object sender, ItemTappedEventArgs e)
        {
            await Navigation.PushAsync(new CreateRestApi(restApi.Id));
        }

        async void Delete(object sender, ItemTappedEventArgs e)
        {
            RestApiDAO.delete(restApi);
            Navigation.PopAsync();
        }
        
        async void Send(object sender, ItemTappedEventArgs e)
        {
            string yes = "YES";
            string no = "NO";
            var answer = await DisplayAlert("Report?", "Send a Report on "+restApi.EMAIL+" ?", yes, no);
            if (answer) {
                try
                {
                    if (restApi == null || graphsPage == null)
                        return;
                    ReportManager reportManager = new ReportManager(restApi, graphsPage.resultStats);
                    await reportManager.sendReportOnMail();
                    await DisplayAlert("Success", "Email sent", "CANCEL");
                }
                catch (Exception es)
                {
                   
                    await DisplayAlert("ERROR", "We could not send mail", "CANCEL");
                }
            }

            
        }
 
        async Task loadResults()
        {
            if (restApi == null)
                return;
            results = new ObservableCollection<Results>(ResultsDAO.getByRestApiId(restApi.Id));
        }
    }
}