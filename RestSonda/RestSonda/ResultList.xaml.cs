﻿using RestSonda.persistence.entities;
using RestSonda.persistence.repository;
using RestSonda.src;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RestSonda
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ResultList : ContentPage
	{
        RestApi restApi;
        ObservableCollection<Results> results = new ObservableCollection<Results>();

        public ResultList ()
		{
			InitializeComponent ();
		}
        public ResultList(RestApi restApi)
        {
            InitializeComponent();
            this.restApi = restApi;
        }
         
        public void init(ObservableCollection<Results> list )
        {
            results = list;
            ResultsList.ItemsSource = results;
        }

        private async void showResponeMessageAsync(object sender, SelectedItemChangedEventArgs e)
        {
            Results result = (Results)((ListView)sender).SelectedItem;
            await DisplayAlert("Response Message",result.RESPONSEMESSAGE + "\n" + result.ERROREMESSAGE, "CANCEL");
        }


    }
}