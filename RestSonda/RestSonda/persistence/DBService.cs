﻿using SQLite;
using RestSonda.persistence.entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;

namespace RestSonda.persistence
{
    class DBService
    {
    

        private static String dbPATH = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "RestSonda.db3");
        public static SQLiteConnection connectToDB() {
            var db = new SQLiteConnection(dbPATH);
            db.CreateTable<RestApi>();
            db.CreateTable<Headers>();
            db.CreateTable<Parameters>();
            db.CreateTable<Results>();
            return db;
        }

        public static SQLiteConnection getDBConnection()
        {
            return new SQLiteConnection(dbPATH);
        }
    }
}
