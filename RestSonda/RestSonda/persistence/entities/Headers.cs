﻿using SQLite;
using System;

namespace RestSonda.persistence.entities
{
    public class Headers : Pair
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int? Id { get; set; }

        /*public int RESTAPIID { get; set; }

        public string KEY { get; set; }
        public string VALUE { get; set; }
        */
        public override string ToString()
        {
            return string.Format("[Person: ID={0}, KEY={1}, VALUE={2}, RESTAPIID={3}", Id, KEY, VALUE, RESTAPIID);
        }
    }
}
