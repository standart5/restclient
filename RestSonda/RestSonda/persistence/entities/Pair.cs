﻿ 
using System;
using System.Collections.Generic;
using System.Text;

namespace RestSonda.persistence.entities
{
    public class Pair
    {
        public int? Id { get; set; }
        public int RESTAPIID { get; set; }

        public string KEY { get; set; }
        public string VALUE { get; set; }
        public String TYPE { get; set; }
    }
}
