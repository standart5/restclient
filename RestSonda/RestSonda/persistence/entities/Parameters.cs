﻿using RestSharp;
using SQLite;
using System;


namespace RestSonda.persistence.entities
{
    public class Parameters : Pair
    {
        [PrimaryKey,AutoIncrement, Column("_id")]
        public int? Id { get; set; }

        /*public int RESTAPIID { get; set; }

        public string KEY { get; set; }
        public string VALUE { get; set; }
        public int TYPE { get; set; }*/

        public override string ToString()
        {
            return string.Format("[Person: ID={0}, KEY={1}, VALUE={2},VALUE={3}, RESTAPIID={4}", Id, KEY, VALUE,TYPE, RESTAPIID);
        }
    }
}
