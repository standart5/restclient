﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestSonda.persistence.entities
{
    [Table("RestApi")]
    public class RestApi
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        public string NAME { get; set; }
        public string URL { get; set; }
        public string METHODE { get; set; }
        public string METHODE_IMAGE { get; set; }
        public string USERNAME { get; set; }
        public string PASSWORD { get; set; }
        public string EMAIL { get; set; }
        public DateTime CREATIONDATE { get; set; }
        public DateTime MODIFYDATE { get; set; }

        public override string ToString()
        {
            return string.Format("[RestApi: ID={0}, URL={1}, USERNAME={2}, CREATIONDATE={3}, MODIFYDATE={4}, NAME={5}, METHODE={6} ", Id, URL,USERNAME, CREATIONDATE, MODIFYDATE, NAME, METHODE);
        }
    }
}
