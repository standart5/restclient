﻿using SQLite;
using System;

namespace RestSonda.persistence.entities
{
    public class Results
    {
        [PrimaryKey,AutoIncrement, Column("_id")]
        public int? Id { get; set; }

        public int RESTAPIID { get; set; }

        public int STATUSCODE { get; set; }
        public string STATUSCODEDESCRIPTION { get; set; }
        public string RESPONSEMESSAGE { get; set; }
        public string ERROREMESSAGE { get; set; }
        public DateTime EXECUTEDATE { get; set; }
        public DateTime ANSWERTIME { get; set; }
        public int WAITINGTIME { get; set; }


        public override string ToString()
        {
            return string.Format("[Person: ID={0}, STATUSCODE={1}, RESPONSEMESSAGE={2},EXECUTEDATE={3}, RESTAPIID={4}", Id, STATUSCODE, RESPONSEMESSAGE, EXECUTEDATE, RESTAPIID);
        }
    }
}
