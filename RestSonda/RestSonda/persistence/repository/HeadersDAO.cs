﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using RestSonda.persistence.entities;

namespace RestSonda.persistence.repository
{
    class HeadersDAO
    {
        public static void create(int restApiId, String key, String value)
        {
            SQLiteConnection db = DBService.connectToDB();
            Headers header = new Headers();
            header.KEY = key;
            header.VALUE = value;
            header.RESTAPIID = restApiId;
             create(header);
        }
        public static int create(Headers header)
        {
            return DBService.connectToDB().Insert(header);
        }
        public static int update(Headers header)
        {
            return DBService.connectToDB().Update(header);
        }

        public static Headers getById(int id)
        {
            return DBService.connectToDB().Get<Headers>(id);
        }

        public static List<Headers> getBy(String field, String value)
        {
            return DBService.connectToDB().Query<Headers>("SELECT * FROM Headers WHERE " + field + " = ?", value);
        }
        public static List<Headers> getByRestApiId(int restApiId)
        {
            return DBService.connectToDB().Query<Headers>("SELECT * FROM Headers WHERE RESTAPIID = ?", restApiId);
        }
        public static TableQuery<Headers> getAll()
        {
            return DBService.connectToDB().Table<Headers>();
        }
        public static int delete(Headers headers)
        {
            return DBService.connectToDB().Delete(headers);
        }
    }
}
