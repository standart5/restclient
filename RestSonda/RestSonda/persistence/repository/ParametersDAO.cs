﻿using System;
using System.Collections.Generic;
using System.Text;
using RestSonda.persistence.entities;
using SQLite;
using RestSharp;

namespace RestSonda.persistence.repository
{
    class ParametersDAO
    {
        public static void create(int restApiId, String key, String value, String type)
        {
            SQLiteConnection db = DBService.connectToDB();
            Parameters parameter = new Parameters();
            parameter.KEY = key;
            parameter.VALUE = value;
            parameter.TYPE = type;
            parameter.RESTAPIID = restApiId;
            create(parameter);
        }

        public static int create(Parameters parameter)
        {
            return DBService.connectToDB().Insert(parameter);
        }

        public static int update(Parameters parameters)
        {
            return DBService.connectToDB().Update(parameters);
        }


        public static Parameters getById(int id)
        {
            return DBService.connectToDB().Get<Parameters>(id);
        }

        public static List<Parameters> getBy(String field, String value)
        {
            return DBService.connectToDB().Query<Parameters>("SELECT * FROM Parameters WHERE " + field + " = ?", value);
        }
        public static List<Parameters> getByRestApiId(int restApiId)
        {
            return DBService.connectToDB().Query<Parameters>("SELECT * FROM Parameters WHERE RESTAPIID = ?", restApiId);
        }
        public static TableQuery<Parameters> getAll()
        {
            return DBService.connectToDB().Table<Parameters>();
        }
        public static int delete(Parameters parameters)
        {
            return DBService.connectToDB().Delete(parameters);
        }
    }
}
