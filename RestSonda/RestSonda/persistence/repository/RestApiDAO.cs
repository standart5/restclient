﻿using RestSonda.persistence.entities;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestSonda.persistence.repository
{
    class RestApiDAO
    {
        public static void create(String name,String methode,String url,String username,String password)
        {
            RestApi restApi = new RestApi();
            restApi.URL = url;
            restApi.USERNAME = username;
            restApi.PASSWORD = password;
            restApi.METHODE = methode;
            restApi.METHODE_IMAGE = methode + ".png";
            restApi.NAME = name;
            restApi.CREATIONDATE = DateTime.Now;
            restApi.MODIFYDATE = DateTime.Now;
            create(restApi);
        }

        public static int create(RestApi restApi) {
            return DBService.connectToDB().Insert(restApi);
        }
        public static int update(RestApi restApi) {
            return DBService.connectToDB().Update(restApi);
        }
        public static int delete(RestApi restApi)
        {
            return DBService.connectToDB().Delete(restApi);
        }
        public static RestApi getById(int id) {
            return DBService.connectToDB().Get<RestApi>(id);    
        }

        public static List<RestApi> getBy(String field,String value) {
            return DBService.connectToDB().Query<RestApi>("SELECT * FROM RestApi WHERE "+field+" = ?", value);
        }

        public static TableQuery<RestApi> getAll() {
            return DBService.connectToDB().Table<RestApi>();
        }

        
    }
}
