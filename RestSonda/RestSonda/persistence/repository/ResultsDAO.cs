﻿using RestSonda.persistence.entities;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestSonda.persistence.repository
{
    class ResultsDAO
    {
        public static void create(int restApiId, int statusCode, String statusCodeDescription, String message,String errorMessage, DateTime executionDate,DateTime answerTime) {
            SQLiteConnection db = DBService.connectToDB();
            Results result = new Results();
            result.STATUSCODE = statusCode;
            result.STATUSCODEDESCRIPTION = statusCodeDescription;
            result.RESPONSEMESSAGE = message;
            result.ERROREMESSAGE = (errorMessage==null)?" ":errorMessage;
            result.EXECUTEDATE = executionDate;
            result.ANSWERTIME = answerTime;
            result.WAITINGTIME = answerTime.Subtract(executionDate).Milliseconds;
            result.RESTAPIID = restApiId;
            
            create(result);
        }
        public static int create(Results results)
        {
            return DBService.connectToDB().Insert(results);
        }

        public static Results getById(int id)
        {
            return DBService.connectToDB().Get<Results>(id);
        }

        public static List<Results> getBy(String field, String value)
        {
            return DBService.connectToDB().Query<Results>("SELECT * FROM Results WHERE " + field + " = ?", value);
        }
        public static List<Results> getByRestApiId(int restApiId)
        {
            return DBService.connectToDB().Query<Results>("SELECT * FROM Results WHERE RESTAPIID = ?", restApiId);
        }
       
        public static List<Results> getByRestApiIdGEDate(int restApiId,DateTime date)
        {
            return DBService.connectToDB().Query<Results>("SELECT * FROM Results WHERE RESTAPIID = ? AND EXECUTEDATE>= ? ORDER BY EXECUTEDATE DESC", restApiId, date);
        }

        public static TableQuery<Results> getAll()
        {
            return DBService.connectToDB().Table<Results>();
        }
    }
}
