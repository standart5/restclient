﻿using RestSonda.persistence.entities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace RestSonda.src
{
    class ReportManager
    {
        private const String TOPIC = "Report for RestApi - {0}";
        private const String FULL_RESULT_MESSAGE_CONTENT = "\n Since - {0} - Status - {1} - Appeared - {2}";
        private const String DAILY_RESULT_MESSAGE_CONTENT = "\n On: - {0} - Status - {1} - Appeared - {2}";
        private const String WEEK_AMOUNT_OF_CONNECTION_MESSAGE_CONTENT = "\n In last - {0} - was done - {1} - queries";
        RestApi restApi;
        ResultStats resultStats;
        private String subject;
        private String body;
        public ReportManager(RestApi restApi, ResultStats resultStats)
        {
            subject = "";
            body = "";
            this.restApi = restApi;
            this.resultStats = resultStats;
        }

        public async Task generateReport() {
            subject = string.Format(TOPIC,restApi.NAME);
            body = subject;
            body += "\n Status For FullTime";
            foreach (var stat in resultStats.resultsPerStatusCode)
            {
                body += string.Format(FULL_RESULT_MESSAGE_CONTENT, restApi.CREATIONDATE, stat.Key, stat.Value);
            }
            body += "\n Status For Day";
            foreach (var stat in resultStats.dailyResultsPerStatusCode)
            {
                body += string.Format(DAILY_RESULT_MESSAGE_CONTENT, DateTime.Now.Date, stat.Key, stat.Value);
            }
            body += "\n Queries for Week";
            foreach (var stat in resultStats.resultsAmountPerWeek)
            {
                body += string.Format(WEEK_AMOUNT_OF_CONNECTION_MESSAGE_CONTENT, stat.Key, stat.Value);
            }
        }
       
        public async Task sendReportOnMail()
        {
            try
            {
                await generateReport();
                var mail = new MailMessage();
                var smtpServer = new SmtpClient("smtp.gmail.com", 587);
                mail.From = new MailAddress("testofmyapplication@gmail.com");
                mail.To.Add(restApi.EMAIL);
                mail.Subject = subject;
                mail.Body = body;
                smtpServer.Credentials = new NetworkCredential("testofmyapplication@gmail.com", "testofmyapplication1994");
                smtpServer.UseDefaultCredentials = false;
                smtpServer.EnableSsl = true;
                smtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
