﻿using RestSharp;
using RestSharp.Authenticators;
using RestSonda.persistence.entities;
using RestSonda.persistence.repository;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace RestSonda.src{

    class RestClientConnector{
        RestClient client;
        RestRequest request;
        RestApi restApi;
        Dictionary<String, ParameterType> parameterTypes;


        public RestClientConnector(RestApi restApi)
        {
            parameterTypes = new Dictionary<string, ParameterType>();
            parameterTypes.TryAdd("Cookie",ParameterType.Cookie);
            parameterTypes.TryAdd("GetOrPost", ParameterType.GetOrPost);
            parameterTypes.TryAdd("HttpHeader", ParameterType.HttpHeader);
            parameterTypes.TryAdd("QueryString", ParameterType.QueryString);
            parameterTypes.TryAdd("RequestBody", ParameterType.RequestBody);
            parameterTypes.TryAdd("UrlSegment", ParameterType.UrlSegment);
            this.restApi = restApi;
            loadClient(restApi.URL, restApi.USERNAME, restApi.PASSWORD);
            loadRequest((restApi.METHODE == "POST") ? Method.POST : Method.GET);
            loadHeaders(HeadersDAO.getByRestApiId(restApi.Id));
            loadParam(ParametersDAO.getByRestApiId(restApi.Id));
        }


        void loadClient(String url,String username, String password)
        {
            try
            {
                client = new RestClient(url);
                if(username!=null && username.Trim()!="" && password != null && password.Trim() != "") client.Authenticator = new HttpBasicAuthenticator(username, password);
            }
            catch (Exception e)
            {
                ResultsDAO.create(restApi.Id,3, "Error", "WRONG URL OR AUTH", e.Message, DateTime.Now, DateTime.Now);
                Console.WriteLine("loadClient");
                throw new Exception("loadClient");
            }
        }
        void loadRequest(Method methode)
        {
            try
            {
                request = new RestRequest(methode);
            }
            catch (Exception e)
            {
                ResultsDAO.create(restApi.Id, 4, "Error", "WRONG REQUEST",e.Message, DateTime.Now, DateTime.Now);
                Console.WriteLine("loadRequest");
                throw new Exception("loadRequest");
            }
        }

        void loadParam(List<Parameters> parameters)
        {
            if (request != null && parameters!=null && parameters.Count>0)
            {
                foreach (Parameters parameter in parameters) {
                    request.AddParameter(parameter.KEY, parameter.VALUE, parameterTypes[parameter.TYPE]);  
                }
            }
        }

       void loadHeaders(List<Headers> headers)
        {
            if (request != null && headers != null && headers.Count > 0)
            {
                foreach (Headers header in headers)
                {
                    request.AddHeader(header.KEY, header.VALUE);
                }
            }
        }

        public async Task  runConnection()
        {
            try
            {
                DateTime executionTime = DateTime.Now;
                client.ExecuteAsync(request, async response =>
                {
                    DateTime answerTime = DateTime.Now;
                    generateResult(response, executionTime, answerTime);
                });
            }
            catch (Exception e)
            {
                ResultsDAO.create(restApi.Id, 1, "ERROR", "runConnection", e.Message, DateTime.Now, DateTime.Now);
                Console.WriteLine("runConnection");
            }
        }

        public async Task generateResult(IRestResponse response,DateTime executionDate,DateTime answerTime)
        {
            try
            {
                if(response!=null  )
                    ResultsDAO.create(restApi.Id,(int)response.StatusCode, response.StatusDescription, response.ResponseStatus.ToString(),response.ErrorMessage, executionDate,answerTime);
                else
                    ResultsDAO.create(restApi.Id, 0,"ERROR", "Response Is Null", "Response Is Null", executionDate, answerTime);
            }
            catch (Exception e)
            {
                ResultsDAO.create(restApi.Id, 1, "ERROR", "generateResult", e.Message, executionDate, answerTime);
                Console.WriteLine("generateResult");
            }
        }
    }
}
