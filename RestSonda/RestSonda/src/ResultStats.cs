﻿using RestSonda.persistence.entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RestSonda.src
{
    public class ResultStats
    {
        Collection<Results> results;
        public Dictionary<String, int> resultsPerStatusCode{ get;set; } //key - StatusDescription value - number of occurrences
        public Dictionary<String, int> dailyResultsPerStatusCode { get; set; }//key - StatusDescription value - number of occurrences
        public Dictionary<String, int> resultsAmountPerWeek { get; set; }//key - DayName value - number of connections
        public List<Results> resultsFromLastTenHours { get; set; } 

        public ResultStats(Collection<Results> results)
        {
            resultsPerStatusCode = new Dictionary<String, int>();
            dailyResultsPerStatusCode = new Dictionary<String, int>();
            resultsAmountPerWeek = new Dictionary<String, int>();
            resultsFromLastTenHours = new List<Results>();
            this.results = results;
            generateStats();
        }

        async void generateStats() {
            foreach (Results result in results)
            {
                await generateResultPerStatusCode(result);
                await generateDailyResultsPerStatusCode(result);
                await generateResultsFromLastTenHours(result);
                await generateResultsAmountPerWeek(result);
            }
        }

        async Task generateResultPerStatusCode(Results result)
        {
            int resultAmount = 0;
            String statusCodeDescritpion = (result.STATUSCODEDESCRIPTION == null)?"":result.STATUSCODEDESCRIPTION;
            if (resultsPerStatusCode.ContainsKey(statusCodeDescritpion))
            {
                resultAmount = resultsPerStatusCode[statusCodeDescritpion];
            }
            resultsPerStatusCode.Remove(statusCodeDescritpion);
            resultsPerStatusCode.TryAdd(statusCodeDescritpion, resultAmount+1);
        }

        async Task generateDailyResultsPerStatusCode(Results result) {
            if(result.EXECUTEDATE.Date!=DateTime.Now.Date)
                return;

            int resultAmount = 0;
            String statusCodeDescritpion = (result.STATUSCODEDESCRIPTION == null) ? "" : result.STATUSCODEDESCRIPTION;
            if (dailyResultsPerStatusCode.ContainsKey(statusCodeDescritpion))
            {
                resultAmount = dailyResultsPerStatusCode[statusCodeDescritpion];
            }
            dailyResultsPerStatusCode.Remove(statusCodeDescritpion);
            dailyResultsPerStatusCode.TryAdd(statusCodeDescritpion, resultAmount+1);
        }

        async Task generateResultsFromLastTenHours(Results result)
        {
           
            //Only from last 10 hours
            if (result.EXECUTEDATE == null || result.EXECUTEDATE < DateTime.Now.AddHours(-10))
                return;
            resultsFromLastTenHours.Add(result);

        }

        async Task generateResultsAmountPerWeek(Results result)
        {
            if (result.EXECUTEDATE == null ||  result.EXECUTEDATE.Date < DateTime.Now.AddDays(-7))
                return;
            //Load information about amount of connection per day from last seven days
            int resultAmount = 1;
            String dayName = result.EXECUTEDATE.DayOfWeek.ToString();
            if (resultsAmountPerWeek.ContainsKey(dayName))
            {
                resultAmount = resultsAmountPerWeek[dayName];
                resultAmount += 1;
             }
            resultsAmountPerWeek.Remove(dayName);
            resultsAmountPerWeek.TryAdd(dayName, resultAmount);
        }
 
    }
}
