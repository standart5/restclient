﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestSonda.src
{
    public class ValidatorUtil
    {
        public  const String EmptyMessage = "{0} Field Can Not Be Empty";
        public  const String ErrorMessage = "{0} Field Value Is Not Correct";

        public static bool isCorrectURL(String url)
        {
            return url.StartsWith("https://") || url.StartsWith("http://");
        }
        public static bool isEmpty(String value)
        {
            return value == null || value.Trim().Equals("");
        }

        public static bool isCorrectEmail(String email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
